package userInterface;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import BancaBLL.Account;
import BancaBLL.Bank;
import BancaBLL.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;






public class ControllerBanca implements Initializable {

    @FXML
    private TextField txtClientID;

    @FXML
    private TextField txtClientName;

    @FXML
    private TextField txtAvailableSum;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnEdit;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnRefresh;

    @FXML
    private TableView<Person> tableClients;

    @FXML
    private TableColumn<Person, String> idClientTab;

    @FXML
    private TableColumn<Person, String> clientNameTab;

    @FXML
    private TableColumn<Person, Double> availableSumClientTab;

    @FXML
    private TableView<Account> tabelAccounts;

    @FXML
    private TableColumn<Account,String> accountIDTab;

    @FXML
    private TableColumn<Account,Account> accountTypeTab;

    @FXML
    private TableColumn<Account,Double> availableAccountSumTab;

    @FXML
    private TextField txtSearch;

    @FXML
    private Button btnSearchClient;

    @FXML
    private MenuItem bankView;

    @FXML
    private MenuItem accountsView;

    @FXML
    private MenuItem documentatie;
    
   private boolean editPressed = false,newPressed = false;
   private static Person persoanaSelectataTabel = new Person();

    public static Person getPersoanaSelectataTabel() {
	return persoanaSelectataTabel;
    }

	@FXML
    void ActionClear(ActionEvent event) {
    	setTextFieldsToNull();
    }

    @FXML
    void ActionDelete(ActionEvent event) {
    	Person personDelete = new Person();
    	textFieldToPerson(personDelete);
    	System.out.println("Nume persoana de delete " + personDelete.getNume());
    	bankInstance.removePerson(personDelete);
    	RefreshAction(event);
    }

    @FXML
    void ActionEdit(ActionEvent event) {
    	txtClientID.setEditable(true);
    	txtClientName.setEditable(true);
    	newPressed = false;
    	editPressed = true;
    	
    	
    }

    @FXML
    void ActionNew(ActionEvent event) {
    	txtClientID.setEditable(true);
    	txtClientName.setEditable(true);
    	editPressed = false;
    	newPressed = true;
    }

    @FXML
    void ActionSave(ActionEvent event) {
    	if(editPressed){
    		Person persoanaUpdatata = new Person();
    		textFieldToPerson(persoanaUpdatata);
    		bankInstance.updatePerson(txtClientName.getText(),persoanaUpdatata);
    	}
    	if(newPressed){
    		Person newPerson = new Person();
    		textFieldToPerson(newPerson);
    		
    		bankInstance.addPerson(newPerson);
    	}
    	editPressed = false;
    	newPressed = false;
    	txtClientID.setEditable(false);
    	txtClientName.setEditable(false);
    	RefreshAction(event);
    }

    @FXML
    private void ClickTableProduseComandate(MouseEvent event) throws IOException {
    	
    	String nume = tableClients.getSelectionModel().getSelectedItem().getNume();
    	 buildAccountsTableViewDataForSelectedPerson(nume);
    	 Person persoanaSelectata = new Person();
    	 persoanaSelectata = bankInstance.findPerson(nume);
    	 setTextFields(persoanaSelectata.getId(), persoanaSelectata.getNume(),persoanaSelectata.getAvailableSum());
    	System.out.println("am detectat clickul");
    	if(event.getClickCount()>1){
    		textFieldToPerson(persoanaSelectataTabel);
    		Functions func = new Functions();
    		func.createStage(Functions.ACCOUNTSVIEW, "Accounts");
    		
    	}
    }

    @FXML
    void ClientiView(ActionEvent event) {
    	
    }

    @FXML
    void ComenziView(ActionEvent event) {

    }

    @FXML
    void RefreshAction(ActionEvent event) {
    	buildTableviewData();
    	buildAccountsTableViewData();
    }

    @FXML
    void openDocumentatie(ActionEvent event) {

    }

    @FXML
    void searchProdus(ActionEvent event) {
    	String numeCautat = txtSearch.getText();
    	Person persoanaCautat = new Person();
    	persoanaCautat = bankInstance.findPerson(numeCautat);
    	setTextFields(persoanaCautat.getId(), persoanaCautat.getNume(), persoanaCautat.getAvailableSum());
    }
    
    private ObservableList<Person> personData;
    private ObservableList<Account> accountData;
    private static Bank bankInstance = Bank.getInstance();
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
        btnDelete.setStyle("-fx-base: red");
        btnClear.setStyle("-fx-base: red");
        btnSave.setStyle("-fx-base: #27ae60");
      

        btnSave.disableProperty().bind(txtClientID.textProperty().isEmpty());

        Tooltip tRefresh = new Tooltip("Refresh Tableview");
        btnRefresh.setTooltip(tRefresh);
        buildTableviewData();	
        buildAccountsTableViewData();
	}
	private void buildTableviewData() {
   	 personData = FXCollections.observableArrayList();
       try {          
			List<Person> rs = new ArrayList<Person>(bankInstance.readAllPersons());
			System.out.println(rs.size());
           for (Person persoana : rs) {
        	   System.out.println("suma in controller" +  persoana.getAvailableSum());
               personData.add(persoana);
           }
       } catch (Exception e) {
           System.out.println(e.getMessage());
       }
       idClientTab.setCellValueFactory(new PropertyValueFactory<>("id"));
       clientNameTab.setCellValueFactory(new PropertyValueFactory<>("nume"));
       availableSumClientTab.setCellValueFactory(new PropertyValueFactory<>("availableSum"));
       tableClients.setItems(personData);

   }
	private void buildAccountsTableViewData(){
		 accountData = FXCollections.observableArrayList();
		
	       try {          
				List<Person> rs = new ArrayList<Person>(bankInstance.readAllPersons());
				List<Account> allAccounts = new ArrayList<Account>();
				allAccounts.addAll(bankInstance.readAccounts());
				
	           for (Account acount : allAccounts) {   
	               accountData.add(acount);
	               System.out.println("suma "+acount.getAvailableAmount() + "type " + acount.getType());
	               
	           }
	       } catch (Exception e) {
	           System.out.println(e.getMessage());
	       }
	       accountIDTab.setCellValueFactory(new PropertyValueFactory<>("id"));
	       accountTypeTab.setCellValueFactory(new PropertyValueFactory<>("type"));
	       availableAccountSumTab.setCellValueFactory(new PropertyValueFactory<>("AvailableAmount"));
	       
	       tabelAccounts.setItems(accountData);
		
	}
	private void buildAccountsTableViewDataForSelectedPerson(String nameOfPerson){
		 accountData = FXCollections.observableArrayList();
		
	       try {          
				
				List<Account> allAccounts = new ArrayList<Account>();
				allAccounts.addAll(bankInstance.findAccountsForPerson(nameOfPerson));
				
	           for (Account acount : allAccounts) {   
	               accountData.add(acount);
	               System.out.println("suma "+acount.getAvailableAmount() + "type " + acount.getType());
	               
	           }
	       } catch (Exception e) {
	           System.out.println(e.getMessage());
	       }
	       accountIDTab.setCellValueFactory(new PropertyValueFactory<>("id"));
	       accountTypeTab.setCellValueFactory(new PropertyValueFactory<>("type"));
	       availableAccountSumTab.setCellValueFactory(new PropertyValueFactory<>("AvailableAmount"));
	       
	       tabelAccounts.setItems(accountData);
		
	}
	
	
	private void setTextFields(String idClient,String numeClient,Double availableSum){
		txtClientID.setText(idClient);
		txtClientName.setText(numeClient);
		txtAvailableSum.setText(availableSum.toString());
	}
	private void setTextFieldsToNull(){
		txtClientID.clear();
		txtClientName.clear();
		txtAvailableSum.clear();
		
	}
	private void textFieldToPerson(Person person){
		person.setId(txtClientID.getText());
		person.setNume(txtClientName.getText());
		person.setAvailableSum(0.00);
	}
	

}
