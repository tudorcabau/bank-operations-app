package userInterface;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Functions {
	
	public static final String GENERALVIEW ="/userInterface/BancaGeneral.fxml";
	public static final String ACCOUNTSVIEW ="/userInterface/AccountsView.fxml";
	
	public void createStage(String fxml,String title) throws IOException{
		Parent root = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(title);
        stage.show();

	}
	
}
