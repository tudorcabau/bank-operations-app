package userInterface;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import BancaBLL.Account;
import BancaBLL.Bank;
import BancaBLL.Person;
import BancaBLL.SavingAccount;
import BancaBLL.SpendingAccount;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;


public class ControllerAccounts implements Initializable {
	
	
	 private Person persoanaCorespunzatoareConturilor = new Person();
	 private ObservableList<Account> accountData;
	 private static Bank bankInstance = Bank.getInstance();
	 private boolean editPressed = false,newPressed = false;

    @FXML
    private TextField txtClientID;

    @FXML
    private TextField txtClientName;

    @FXML
    private TextField txtAvailableSum;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnEdit;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private TextField txtAccountID;

    @FXML
    private TextField txtAccountAvailableSum;

    @FXML
    private RadioButton btnRadioSpending;

    @FXML
    private ToggleGroup toggleAccount;

    @FXML
    private RadioButton btnRadioSaving;

    @FXML
    private TextField txtSumaDepunere;

    @FXML
    private TextField txtSumaRetragere;

    @FXML
    private Button btnDepunere;

    @FXML
    private Button btnRetragere;

    @FXML
    private Button btnRefresh;

    @FXML
    private TableView<Account> tabelAccounts;

    @FXML
    private TableColumn<Account,String> accountIDTab;

    @FXML
    private TableColumn<Account,Account> accountTypeTab;

    @FXML
    private TableColumn<Account,Double> availableAccountSumTab;
    
    @FXML
    private MenuItem bankView;

    @FXML
    private MenuItem accountsView;

    @FXML
    private MenuItem documentatie;

    @FXML
    private void ActionClear(ActionEvent event) {
    	setTextFieldsToNull();
    }

    @FXML
    private void ActionDelete(ActionEvent event) {
    	Account accountDelete;
    	accountDelete = textFieldToAccount();
    	bankInstance.removeHolderAccount(accountDelete, persoanaCorespunzatoareConturilor);
    	buildAccountsTableViewDataForSelectedPerson();
    }

    @FXML
    private void ActionDepunere(ActionEvent event) {
    	Account accountDepunere = textFieldToAccount();
    	bankInstance.writeAccountData(accountDepunere,Double.parseDouble(txtSumaDepunere.getText()), persoanaCorespunzatoareConturilor);
    	buildAccountsTableViewDataForSelectedPerson();
    }

    @FXML
    void ActionEdit(ActionEvent event) {
    	txtAccountID.setEditable(true);
    	newPressed = false;
    	editPressed = true;
    }

    @FXML
    void ActionNew(ActionEvent event) {
    	txtAccountID.setEditable(true);
    	editPressed = false;
    	newPressed = true;
    }

    @FXML
    void ActionRetragere(ActionEvent event) {
    	Account accountRetragere = textFieldToAccount();
    	bankInstance.writeAccountData(accountRetragere,- Math.abs((Double.parseDouble(txtSumaRetragere.getText()))), persoanaCorespunzatoareConturilor);
    	buildAccountsTableViewDataForSelectedPerson();
    
    }

    @FXML
    void ActionSave(ActionEvent event) {
    	if(editPressed){
    		Account accountUpdatat = textFieldToAccount();
    		bankInstance.updateAccount(persoanaCorespunzatoareConturilor.getNume(), txtAccountID.getText(), accountUpdatat);
    	}
    	if(newPressed){
    		Account newAccount = textFieldToAccount();
    		bankInstance.addHolderAccount(newAccount, persoanaCorespunzatoareConturilor);
    	}
    	editPressed = false;
    	newPressed = false;
    	txtClientID.setEditable(false);
    	txtClientName.setEditable(false);
    	//RefreshAction(event);
    	buildAccountsTableViewDataForSelectedPerson();
    }

    @FXML
   private void ActionTableAccountsClicked(MouseEvent event) {
    	String id = tabelAccounts.getSelectionModel().getSelectedItem().getId();
       	Double sum = tabelAccounts.getSelectionModel().getSelectedItem().getAvailableAmount();
       
       	txtAccountID.setText(id);
       	//txtAccountAvailableSum.setEditable(true);
       	txtAccountAvailableSum.setText(sum.toString());
       	System.out.println("am detectat clickul");
    }

    @FXML
    void ClientiView(ActionEvent event) {

    }

    @FXML
    void ComenziView(ActionEvent event) {

    }

    @FXML
    void RefreshAction(ActionEvent event) {
    	buildAccountsTableViewDataForSelectedPerson();
    }

    @FXML
    void actionRadioButtonSavingAccount(ActionEvent event) {

    }

    @FXML
    void actionRadioButtonSpendingAccount(ActionEvent event) {

    }

    @FXML
    void openDocumentatie(ActionEvent event) {

    }
    
    
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		persoanaCorespunzatoareConturilor = ControllerBanca.getPersoanaSelectataTabel();
        btnDelete.setStyle("-fx-base: red");
        btnClear.setStyle("-fx-base: red");
        btnSave.setStyle("-fx-base: #27ae60");
        txtSumaDepunere.setEditable(true);
        txtSumaRetragere.setEditable(true);

        btnSave.disableProperty().bind(txtAccountID.textProperty().isEmpty());

        Tooltip tRefresh = new Tooltip("Refresh Tableview");
        btnRefresh.setTooltip(tRefresh);
        txtClientID.setText(persoanaCorespunzatoareConturilor.getId());
		txtClientName.setText(persoanaCorespunzatoareConturilor.getNume());
		txtAvailableSum.setText(persoanaCorespunzatoareConturilor.getAvailableSum().toString());
        buildAccountsTableViewDataForSelectedPerson();	
		
	}
	private void buildAccountsTableViewDataForSelectedPerson(){
  		 accountData = FXCollections.observableArrayList();
  		
  	       try {          
  				
  				ArrayList<Account> allAccounts = new ArrayList<Account>();
  				allAccounts.addAll(bankInstance.findAccountsForPerson(persoanaCorespunzatoareConturilor.getNume()));
  				
  	           for (Account acount : allAccounts) {   
  	               accountData.add(acount);
  	               System.out.println("suma "+acount.getAvailableAmount() + "type " + acount.getType());
  	               
  	           }
  	       } catch (Exception e) {
  	           System.out.println(e.getMessage());
  	       }
  	       accountIDTab.setCellValueFactory(new PropertyValueFactory<>("id"));
  	       accountTypeTab.setCellValueFactory(new PropertyValueFactory<>("type"));
  	       availableAccountSumTab.setCellValueFactory(new PropertyValueFactory<>("AvailableAmount"));
  	       
  	       tabelAccounts.setItems(accountData);
  		
  	}
	private void setTextFieldsToNull(){
		txtAccountID.clear();
		txtAccountAvailableSum.clear();
		
	}
	private  Account textFieldToAccount(){
		Account newAccount ;
		if(btnRadioSaving.isSelected()){
			
		newAccount = new SavingAccount(txtAccountID.getText(), 0);
		
		//newAccount.setAvailableAmount(Double.parseDouble(txtAccountAvailableSum.getText()));
		System.out.println("returnez saving account");
		return newAccount;
		}
		if(btnRadioSpending.isSelected()){
			newAccount = new SpendingAccount(txtAccountID.getText(), 0);
    		//newAccount.setAvailableAmount(Double.pareDouble(txtAccountAvailableSum.getText()));
			System.out.println("returnez spending account");
    		return newAccount;
		}
		
		return null;
	}
 

}
