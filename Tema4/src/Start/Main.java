package Start;


import BancaBLL.Bank;
import BancaBLL.Person;

import java.io.IOException;
import java.util.ArrayList;

import BancaBLL.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
	public class Main extends Application{
		@Override
		public void start(Stage primaryStage) throws Exception{
		Parent root = FXMLLoader.load(getClass().getResource("/userInterface/BancaGeneral.fxml"));
		//primaryStage.setTitle("Aplicatie");
		primaryStage.setScene(new Scene(root));
		primaryStage.show();		
		}
	
	public static void main(String [] args){
	Bank banca = Bank.getInstance();
//	System.out.println("lala");
//	Person pers = new Person("1","Redardat");
//	SavingAccount acc = new SavingAccount("1",10);
//	
//	ArrayList<Account> listaConturi = new ArrayList<Account>();
//	listaConturi.add(acc);
//	banca.addPerson(pers);
//	banca.addHolderAccount(acc, pers);
//	System.out.println(acc.getAvailableAmount());
//	System.out.println("nr conturi pers main " + banca.getHash().get(pers).size());
//	//banca.readAccountData(acc, pers);
//	//banca.removeHolderAccount(acc, pers);
//	//acc.setAvailableAmount(20);
//	//acc.retragere(2);
//	banca.readAccountData(acc, pers);
//	try {
//		banca.writeToFile();
//	} catch (IOException e) {
//		System.out.println(e.getMessage());
//	}
//	try {
//		banca.readFile();
//	} catch (ClassNotFoundException | IOException e) {
//	System.out.println(e.getMessage());
//	}
		try {
			banca.readFile();
			System.out.println("am citit din fisier");
		} catch (ClassNotFoundException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		launch(args);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
		    public void run() {
		        try {
					banca.writeToFile();
					System.out.println("am scris in fisier ");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		}));	
	}
	
}
