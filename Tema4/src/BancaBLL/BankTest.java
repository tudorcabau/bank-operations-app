package BancaBLL;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class BankTest {
	
	private static Bank bankInstance = Bank.getInstance();
	private static Person person = new Person("1","Popescu");
	private static Account account = new SavingAccount("2", 200);
	private static Account account1 = new SavingAccount("2", 200);
	private static ArrayList<Account> listaConturi = new ArrayList<Account>();
	static{
		listaConturi.add(account);
	}
	
	

//	@Test
//	public void testOrder() {testAddPerson();testAddHolderAccount();testReadAccountData();testWriteAccountData();testRemoveHolderAccount();testRemovePerson(); }
	
	/*
	 * Numele testelor incep cu o litera care reprzinta ordinea de testare alfabeitca
	 * */
	
	@Test
	public void atestAddPerson() {
		
		bankInstance.addPerson(person);
	}

	@Test
	public void btestAddHolderAccount() {
		bankInstance.addHolderAccount(account1, person);
	}
	
	@Test
	public void ftestRemovePerson() {
		bankInstance.removePerson(person);
	}
	
	@Test
	public void etestRemoveHolderAccount() {
		bankInstance.removeHolderAccount(account1, person);
	}

	@Test
	public void ctestReadAccountData() {
		bankInstance.readAccountData(account, person);
	}

	@Test
	public void dtestWriteAccountData() {
		bankInstance.writeAccountData(account,20.00, person);
	}

	@Test
	public void testReportGenerator() {
		//System.out.println("Not yet implemented");
	}

	@Test
	public void testWriteToFile() {
		//System.out.println("Not yet implemented");
	}

	@Test
	public void testReadFile() {
		//System.out.println("Not yet implemented");
	}



}
