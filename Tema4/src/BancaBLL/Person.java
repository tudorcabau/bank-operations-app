package BancaBLL;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
public class Person implements Serializable,Observer {
	private String id;
	private String nume;
	//private Date dataNasterii;
	private String password;
	private Double availableSum;

	
	public Person(String id, String nume) {
		super();
		this.id = id;
		this.nume = nume;
		updateAvailableSum();
	}
	public Person(){}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getNume() {
		return nume;
	}



	public void setNume(String nume) {
		this.nume = nume;
	}


	@Override
	public String toString() {
		return "Person [id=" + id + ", nume=" + nume + ", password=" + password + ", availableSum=" + availableSum
				+ "]";
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public Double getAvailableSum() {
		return availableSum;
	}



	public void setAvailableSum(Double availableSum) {
		this.availableSum = availableSum;
	}
	
	 @Override
	     public void update(Observable observable, Object arg)
	     {
	            updateAvailableSum();
	            if(arg.getClass().isInstance(String.class)){
	            System.out.println("Extras din contul lui  " + this.nume +" "+ arg);
	            }
	            
	     }

	 public void updateAvailableSum(){
		 this.availableSum = Bank.updatePersonDebit(this);
		 
	 }
	
	@Override
	public boolean equals(Object other){
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Person))
	    	{return false;}
	    Person persoana = (Person)other;
	    return //persoana.equals(id)&&
	    		persoana.nume.equals(this.nume); //&&
	    		//persoana.password == password;
	    
		//return false;
		}
	
	@Override
	public int hashCode(){
		int result = 17;
		result = 31 * result + id.hashCode();
        result = 31 * result + nume.hashCode();
       // result = 31 * result + dataNasterii.hashCode();
        return result;
		
	}
}
