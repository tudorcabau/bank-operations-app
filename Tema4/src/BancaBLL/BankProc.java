package BancaBLL;

import java.util.ArrayList;

public interface BankProc {
	
/*
 * 	Precondition:person != null && account !=null
 * 	Postcondition:HashMap.size == HashMap.size()@Precondition + 1;
*/
	public void addPerson(Person person);
	
	/*
	 * 	Precondition:person != null 
	 * 	Postcondition:HashMap.size == HashMap.size()@Precondition - 1;
	*/
	public void removePerson(Person person);
	
	/*
	 * 	Precondition:person != null && account !=null
	 * 	Postcondition:HashMap.get(person).size() == HashMap.get(person).size()@Precondition + 1;
	*/
	public void addHolderAccount(Account account,Person person);
	
	/*
	 * 	Precondition:person != null && account !=null
	 * 	Postcondition:HashMap.get(person).size() == HashMap.get(person).size()@Precondition - 1;
	*/
	public void removeHolderAccount(Account account,Person person);
	
	/*
	 * 	Precondition:person != null && account !=null
	 * 	Postcondition:HashMap.get(person).size() == HashMap.get(person).size()@Precondition;
	*/
	public void readAccountData(Account account,Person person);
	
	/*
	 * 	Precondition:suma != null && account !=null
	 * 	Postcondition:HashMap.get(person).get(account).getAvailableAmount() == HashMap.get(person).get(account).getAvailableAmount() + suma
	*/
	public void writeAccountData(Account account,Double suma,Person person);
	
	public void reportGenerator();


	
}
