package BancaBLL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class SavingAccount extends Account implements Serializable {
	private static final double COMISION = 0.2;
	//Date[] dataRetrageri = new Date[2];
	private ArrayList<Date> dataRetragerii = new ArrayList<Date>();
	public SavingAccount(String id, double availableAmount) {
		super(id,availableAmount);

	}
	@Override
	public String depunere(Double suma){
		//super.updateAvailableAmount(suma);
		System.out.println("am apelat saving account depunere");
		super.setAvailableAmount(super.getAvailableAmount() + suma);
		return "Ati depus cu succes suma de" + suma +"Suma noua dinsponibila"+ super.getAvailableAmount();
	}
	public String retragere(int suma){
		
		if(super.enoughFunds(suma)){
		if(isWithdrawelAllowed()){
			
			double comision = suma * COMISION;
			double sumaExtrasa = - suma - comision;
			super.setAvailableAmount(getAvailableAmount() + sumaExtrasa);
			return "Ati extras cu succes suma de" + suma+" Comisionul perceput: "+comision +"Suma noua dinsponibila"+ super.getAvailableAmount();
		}
		return "Nu este permisa o noua retragere"; 
		}
	return "Fonduri insuficiente";
	}
	public boolean isWithdrawelAllowed(){
		Date dataCurenta = new Date();
		
		for(Date d : dataRetragerii){
			long timpTrecut = dataCurenta.getTime() - d.getTime();
			if(timpTrecut > 600000 ){
				dataRetragerii.remove(d);
				return true;
			}
		}
		if(dataRetragerii.size()>=4){
			return false;
		}
		return true;
	}
	
	
}
