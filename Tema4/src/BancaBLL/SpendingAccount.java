package BancaBLL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class SpendingAccount extends Account implements Serializable {
	
	//ArrayList<Date> dataRetragerii = new ArrayList<Date>();
	public SpendingAccount(String id, double availableAmount) {
		super(id,availableAmount);

	}
	@Override
	public String depunere(Double suma){
		//super.updateAvailableAmount(suma);
		System.out.println("am apelat spending account depunere");
		super.setAvailableAmount(getAvailableAmount() + suma);
		return "Ati depus cu succes suma de" + suma +"Suma noua dinsponibila"+ super.getAvailableAmount();
	}
	
	public String retragere(int suma){
			double sumaExtrasa = - suma;
			if(super.enoughFunds(suma)){
			super.setAvailableAmount(getAvailableAmount() + sumaExtrasa);
			return "Ati extras cu succes suma de" + suma +"Suma noua dinsponibila"+ super.getAvailableAmount();
			}
		return "Fonduri insuficiente";
	
	}

}
