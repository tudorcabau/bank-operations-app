package BancaBLL;

import java.io.Serializable;
import java.util.Observable;

import org.w3c.dom.views.AbstractView;

public abstract class Account extends Observable implements Serializable {
	private String id;
	//private Person person;
	private double availableAmount = 0;
	private String type;
	public Account(){
	}
//	public Account(String id,Person person, double availableAmound) {
//		super();
//		this.id = id;
//		this.person = person;
//		this.availableAmount = availableAmound;
//	}
	public Account(String id, double availableAmound) {
		super();
		this.id = id;
		this.availableAmount = availableAmound;
		this.type = this.getClass().getSimpleName();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
//	public Person getPerson() {
//		return person;
//	}
//	public void setPerson(Person person) {
//		this.person = person;
//	}
	
	@Override
	public String toString() {
		return "Account [id=" + id + ", availableAmount=" + availableAmount + "]";
	}
	
	public double getAvailableAmount() {
		return availableAmount;
	}
	public void setAvailableAmount(double availableAmount) {
		this.availableAmount = availableAmount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void updateAvailableAmount(Double suma){
		depunere(suma);
		//this.availableAmount += suma; 
		setChanged();
		String detaliCont = new String();
		detaliCont = "Tip cont " + this.getClass().getSimpleName()+", " + this.toString() ;
		notifyObservers(detaliCont);
		
	}
	
	public boolean enoughFunds(int suma){
		if ((availableAmount - suma)>=0)
			return true;
		return false;
	}
//	public String depunere(Double suma){
//		
//	}
//	@Override
//	public boolean equals(Object other){
//		if (other == null) return false;
//	    if (other == this) return true;
//	    if (!(other instanceof Account))return false;
//	    Account account = (Account)other;
//	    return account.equals(id)&&
//	    		account.person == person &&
//	    		account.availableAmount == availableAmount;
//	    
//		//return false;
//		}
//	
//	@Override
//	public int hashCode(){
//		int result = 17;
//		result = 31 * result + id.hashCode();
//        result = 31 * result + person.hashCode();
//        result = (int) (31 * result + availableAmount);
//        return result;
//		
//	}
	public String depunere(Double suma) {
		System.out.println("este apelata metoda superclasei de depunere");
		return null;
	}
}