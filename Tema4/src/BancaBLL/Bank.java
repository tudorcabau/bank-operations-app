package BancaBLL;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.AccountNotFoundException;

import javafx.beans.Observable;

public class Bank implements BankProc, Serializable {
	private static Hashtable<Person, ArrayList<Account>> hash = new Hashtable<Person, ArrayList<Account>>();
	
	
		 
		private static Bank instance = null;
		   protected Bank() {
		      // Exists only to defeat instantiation.
		   }
		   public static Bank getInstance() {
		      if(instance == null) {
		         instance = new Bank();
		      }
		      return instance;
		   }
		
	
	@Override
	public void addPerson(Person person) {
		assert !hash.containsKey(person);
		ArrayList<Account> arrayList = new ArrayList<Account>();
		if(!hash.containsKey(person)){
		hash.put(person, arrayList);
		}
		assert hash.containsKey(person);
		
	}

	@Override
	public void removePerson(Person person) {
		assert hash.containsKey(person);
		int sizePrecondtion = hash.size();
		System.out.println("Marime hash inainte de remove"+sizePrecondtion);
		if(hash.containsKey(person)){
		hash.remove(person);
		}
		else{
			System.out.println("Nu am gasit persoana de deletat");
		}
		int sizePoscondition = hash.size();
		System.out.println("Marime hash dupa remove" + sizePoscondition);
		assert !hash.containsKey(person);
		
	}

	@Override
	public void addHolderAccount(Account account, Person person) {
		assert hash.containsKey(person);
		
//		if (hash.containsKey(person)){
//			System.out.println("marime array inainte de a agauga contul" + Bank.hash.get(person).size() );
//			Bank.hash.get(person).add(account);
//			System.out.println("marime array dupa de a agauga contul" + Bank.hash.get(person).size() );
//			account.addObserver(person);
//			person.updateAvailableSum();
//		}else{
//			System.out.println("hashmap-ul nu contine persoana");
//		}
		ArrayList<Account> arrayList = new ArrayList<Account>();
		Set<Person> keys = hash.keySet();
		   for(Person pers : keys){
			   if (pers.getNume().toLowerCase().equals(person.getNume().toLowerCase())){
				   //arrayList.add(account);
				  // hash.put(person, arrayList);
				   hash.get(person).add(account);
				   System.out.println("cred ca am aduagat contul");
			   }
			   else{
				   System.out.println("nu am gasit persoana");
			   }
		   }
		  
		   
		assert hash.get(person).contains(account);
	}

	@Override
	public void removeHolderAccount(Account account, Person person) {
		assert (hash.get(person).contains(account));
		int sizePrecondtion = hash.get(person).size();
		System.out.println("nr conturi" + sizePrecondtion);
		ArrayList<Account> conturiPersoana = hash.get(person);
		System.out.println("size array conturi persoana update cont " + conturiPersoana.size());
		for(Account acc : conturiPersoana){
			if(acc.getId().equals( account.getId())){
				System.out.println("Am gasit contul in care dupen");
				conturiPersoana.remove(account);
			}
				
		}
		hash.get(person).clear();
		hash.get(person).addAll(conturiPersoana);
		assert !(hash.get(person).contains(account));
	}

	@Override
	public void readAccountData(Account account, Person person) {
		assert person!=null && account != null;
		int sizePrecondtion = hash.get(person).size();
		ArrayList<Account> conturiPersoana = hash.get(person);
		System.out.println("nr conturi " + conturiPersoana.size() + " persoana " );
		int sizePoscondition = hash.get(person).size();
		assert sizePoscondition == sizePrecondtion;
	}

	@Override
	public void writeAccountData(Account account,Double suma,Person person) {
		assert suma != 0 && account != null;
		double sumaPreconditie = account.getAvailableAmount();
		ArrayList<Account> conturiPersoana = hash.get(person);
		System.out.println("size array conturi persoana update cont " + conturiPersoana.size());
		for(Account acc : conturiPersoana){
			if(acc.getId().equals( account.getId())){
				System.out.println("Am gasit contul in care dupen");
				acc.updateAvailableAmount(suma);
			}
				
		}
		double sumaPostconditie = account.getAvailableAmount();
		assert sumaPostconditie == sumaPreconditie + suma;
		
	}

	@Override
	public void reportGenerator() {
		// TODO Auto-generated method stub
		
	}
	public static void writeToFile() throws IOException{
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Banca.bin"));
		objectOutputStream.writeObject(hash);
		
	}
	public static void readFile() throws IOException,ClassNotFoundException{
		ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("Banca.bin"));
		Hashtable<Person, ArrayList<Account>> readObject = (Hashtable<Person, ArrayList<Account>>) objectInputStream.readObject();
		hash = readObject;
	}
	public static ArrayList<Person> readAllPersons(){
		ArrayList<Person> array = new ArrayList<Person>();
		Set<Person> keys = hash.keySet();
	    Iterator<Person> itr = keys.iterator();
	 
	    while (itr.hasNext()) { 
	        array.add(itr.next());
	    }
	    
	    return array;
	}
	public static ArrayList<Account> readAccounts(){
		ArrayList<Person> array = new ArrayList<Person>();
		ArrayList<Account> accountArry = new ArrayList<Account>();
		Set<Person> keys = hash.keySet();
	    Iterator<Person> itr = keys.iterator();
	    
	    //System.out.println("nr conturi clasa banca " + hash.get(0).size());
	    while (itr.hasNext()) { 
	    	
	        accountArry.addAll(hash.get(itr.next()));
	    }
	    System.out.println("nr conturi clasa banca " + accountArry.size());
	    return accountArry;
	}
	
	public Person findPerson(String name){
		//ArrayList<Person> array = new ArrayList<Person>();
		Person persoanaCautata = new Person();
		Set<Person> keys = hash.keySet();
	   for(Person pers : keys){
		   if (pers.getNume().toLowerCase().equals(name.toLowerCase())){
			   persoanaCautata = pers;
			   return persoanaCautata;
		   }
	   }
	    return persoanaCautata;
	}
	public ArrayList<Account> findAccountsForPerson(String name){
		ArrayList<Account> conturiPersoana = new ArrayList<Account>();
		Set<Person> keys = hash.keySet();
	   for(Person pers : keys){
		   if (pers.getNume().toLowerCase().equals(name.toLowerCase())){
			   conturiPersoana.addAll(hash.get(pers));
		   }
	   }
	   System.out.println("am apelat find accounts marime array " + conturiPersoana.size());
	    return conturiPersoana;
	}
	public void updatePerson(String name,Person persoanaUpdatata ){
		 
		Set<Person> keys = hash.keySet();
	   for(Person pers : keys){
		   if (pers.getNume().toLowerCase().equals(name.toLowerCase())){
			   pers.setId(persoanaUpdatata.getId());
			   pers.setNume(persoanaUpdatata.getNume());
			   pers.setPassword(persoanaUpdatata.getPassword());
			   
		   }
	   }
		
	}
	public void updateAccount(String persoanaUpdatata,String idAccount,Account accountUpdatat ){
	  ArrayList<Account> arrayAccounts = hash.get(persoanaUpdatata);
	  for(Account acc : arrayAccounts){
		   if (acc.getId().equals(idAccount)){
			   acc.setId(accountUpdatat.getId());
			   acc.setAvailableAmount(accountUpdatat.getAvailableAmount());
		   }
	  }
	}
	public static Double updatePersonDebit(Person caller){
		ArrayList<Account> callerAccounts = new ArrayList<Account>();
		double callerDebit =  0;
		if((callerAccounts = hash.get(caller)) != null){
		for(Account account :  callerAccounts){
			callerDebit = callerDebit + account.getAvailableAmount();
		}
		}
		return callerDebit;
	}
	  public static Hashtable<Person, ArrayList<Account>> getHash() {
			return hash;
		}
		public static void setHash(Hashtable<Person, ArrayList<Account>> hash) {
			Bank.hash = hash;
		}
	
	


}
